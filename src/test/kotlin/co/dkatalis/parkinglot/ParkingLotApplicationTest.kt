package co.dkatalis.parkinglot

import io.mockk.every
import io.mockk.mockkStatic
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

internal class AtmApplicationTest {

    private val parkingLot = ParkingLotApplication()

    @Test
    fun `readInput() - Given input - Should capture input`() {
        // Given
        val expected = UUID.randomUUID().toString()

        mockkStatic("kotlin.io.ConsoleKt")
        every { readln() } returns expected

        // When
        val actual = parkingLot.readInput()

        // Then
        assertEquals(expected, actual)
    }
}

